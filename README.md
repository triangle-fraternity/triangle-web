# Triangle-web

This is the frontend for the website. It will handle all of the UX for our web
apps. The backend supporting triangle-web is triangle-api.

## Prerequisites

You will need the following things properly installed on your computer.
* Git
* Node (6.3.1) and NPM (whatever NVM installs with 6.3.1)
* Bower (run `npm install -g bower`)
* Ember CLI (run `npm install -g ember-cli@2.7.0`)
* PhantomJS (run `npm install -g phantomjs-prebuilt`)
* Surge (run `npm install -g surge`)

## Installation

* git clone this repo into the same directory you will be cloning triangle-api
* `cd triangle-web`
* `npm install`
* `bower install`

## Running / Development

* `ember s` to serve on localhost:4200 (be sure to have triangle-api running
OR
* `ember s --environment=production` to server on localhost:4200 but hit prod api

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

* `rm -rf dist`
* `ember build --environment production`
* `cd dist`
* `cp index.html 200.html`
* `echo illinoistriangle.com > CNAME`
* `surge`

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

