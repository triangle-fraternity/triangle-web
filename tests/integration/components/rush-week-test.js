import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('rush-week', 'Integration | Component | rush week', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.set('week', [{
    week: 1,
    image:'wings.jpg',
    info_title:'event 1',
    info:'Come join us for brother-made hot wings and onion rings, Tuesday (1/19) @ 7pm',
    reveal_title:'Wings and Rings',
    reveal:'There will be several different flavors. Last year there were 3.\nFollowing/During the end of the event, we will be watching the Blackhawks game.'
  },
  {
    week: 1,
    image:'wings.jpg',
    info_title:'event 2',
    info:'Come join us for brother-made hot wings and onion rings, Tuesday (1/19) @ 7pm',
    reveal_title:'Wings and Rings',
    reveal:'There will be several different flavors. Last year there were 3.\nFollowing/During the end of the event, we will be watching the Blackhawks game.'
  }]);

  this.render(hbs`{{rush-week events=week}}`);
  assert.ok(true);
  //
  // assert.equal(this.$().text().trim(), '');
  //
  // // Template block usage:
  // this.render(hbs`
  //   {{#rush-week}}
  //     template block text
  //   {{/rush-week}}
  // `);
  //
  // assert.equal(this.$().text().trim(), 'template block text');
});
