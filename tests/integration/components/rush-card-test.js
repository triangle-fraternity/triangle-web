import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('rush-card', 'Integration | Component | rush card', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  // Template block usage:
  this.render(hbs`
    {{#rush-card}}
      template block text
    {{/rush-card}}
  `);
  assert.ok(true);
  //
  // assert.equal(this.$().text().trim(), 'template block text');
});
