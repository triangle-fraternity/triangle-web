import Ember from 'ember';

export default Ember.Component.extend({
  name: Ember.computed('brother', function() {
    return this.get('brother.name');
  }),
  initials: Ember.computed('brother', function() {
    return this.get('brother.initials');
  })
});
