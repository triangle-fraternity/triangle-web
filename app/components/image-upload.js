import Ember from 'ember';
import config from 'triangle-web/config/environment';

export default Ember.Component.extend(window.Droplet, {
  options: {
    maximumSize: 250000
  },

  url: config.APP.host + '/images',

  allReady: Ember.computed('validFiles', function() {
    return Ember.isEqual(1, this.get('validFiles').length);
  }),

  hooks: {
    didUpload: function() {
      Ember.Logger.log("Did an upload!!!");
    }
  }
});
