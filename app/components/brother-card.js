import Ember from 'ember';
import config from 'triangle-web/config/environment';

export default Ember.Component.extend({
  host: config.APP.imageHost,
  position: Ember.computed('execb', 'idx', function() {
    return this.get('execb')[this.get('idx')];
  }),
  title: Ember.computed('position', function() {
    return this.get('position.title');
  }),
  brother: Ember.computed('position', function() {
    return this.get('position.brother_id');
  }),
  name: Ember.computed('brother', function() {
    return this.get('brother.name');
  }),
  image: Ember.computed('brother', function() {
    return this.get('brother.image_id');
  }),
  url: Ember.computed('image', function() {
    return this.get('image.url');
  })

});
