import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return Ember.RSVP.hash({
      rushEvents: this.get('store').findAll('rush-event'),
      jobs: this.get('store').findAll('job'),
      brothers: this.get('store').findAll('brother'),
      images: this.get('store').findAll('image')
    });
  }
});
