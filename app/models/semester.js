import DS from 'ember-data';

export default DS.Model.extend({
  year: DS.attr('integer'),
  term: DS.attr('string'),
  brothers: DS.hasMany('brother')
});
