import DS from 'ember-data';

export default DS.Model.extend({
  first_name: DS.attr('string'),
  middle_name: DS.attr('string'),
  last_name: DS.attr('string'),
  major: DS.attr('string'),
  pledge_class: DS.attr('string'),
  initials: DS.attr('string'),
  exec: DS.attr('number'),
  positions: DS.attr('string'),
  job_id: DS.belongsTo('job')
});
