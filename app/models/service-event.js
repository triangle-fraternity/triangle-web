import DS from 'ember-data';

export default DS.Model.extend({
  servicePod: DS.belongsTo('service-pod'),
  semester: DS.belongsTo('semester'),
  description: DS.attr('string'),
  duration: DS.attr('float')
});
