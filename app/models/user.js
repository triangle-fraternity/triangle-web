import DS from 'ember-data';

export default DS.Model.extend({
  brother: DS.belongsTo('brother')
});
