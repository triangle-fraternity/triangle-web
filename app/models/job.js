import DS from 'ember-data';

export default DS.Model.extend({
  title: DS.attr('string'),
  execb: DS.attr('boolean'),
  email: DS.attr('string'),
  brother_id: DS.belongsTo('brother')
});
