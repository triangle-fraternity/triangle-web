import DS from 'ember-data';

export default DS.Model.extend({
  brother: DS.belongsTo('brother'),
  semester: DS.belongsTo('semester'),
  serviceEvents: DS.hasMany('service-event'),
  name: DS.attr('string')
});
