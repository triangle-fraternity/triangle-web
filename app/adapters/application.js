import DS from 'ember-data';
import Ember from 'ember';
import config from 'triangle-web/config/environment';

export default DS.JSONAPIAdapter.extend({
  // Application specific overrides go here
  host: config.APP.host,
  authorizer: 'authorizer:devise',
  pathForType: function(type) {
    return Ember.String.pluralize(Ember.String.underscore(type));
  }
});
