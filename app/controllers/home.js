import Ember from 'ember';

export default Ember.Controller.extend({
  showModal: false,
  execb: Ember.computed('model.jobs', function() {
    const execb = this.get('model.jobs').filter((j) => j.get('execb') > 0);
    return execb.sortBy('execb');
  }),
  actions: {
    openModal() {
      this.set('showModal', true);
    },
    closeModal() {
      this.set('showModal', false);
    },
    agree() {
      this.set('showModal', false);
    }
  }
});
