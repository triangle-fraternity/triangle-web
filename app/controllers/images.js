import Ember from 'ember';
import config from 'triangle-web/config/environment';

export default Ember.Controller.extend({
  host: config.APP.imageHost
});
